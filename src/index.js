import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Footer from "./components/Footer/Footer";
import Archive from "./components/Archive/Archive"
import Header from "./components/Header/Header";

const routing = (
  <Router>
    <div>
    <Header/>
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/archive" component={Archive} />
      </Switch>
      <Footer />
    </div>
  </Router>
);

ReactDOM.render(routing, document.getElementById("root"));