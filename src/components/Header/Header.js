
import React from "react";
import { NavLink } from "react-router-dom";
import {MdOutlineArchive} from 'react-icons/md'
import {BiHomeAlt} from 'react-icons/bi'

function Header() {
  return (
    <nav>
    <h1>Keeper</h1>
      <NavLink className="LinkButton" exact activeClassName="selected" to="/">
        <BiHomeAlt size={20} />Home
      </NavLink>
      <NavLink className="LinkButton" activeClassName="inactive" to="/Archive">
        <MdOutlineArchive size={20} />Archive
      </NavLink>
    </nav>
  );
}
export default Header;
